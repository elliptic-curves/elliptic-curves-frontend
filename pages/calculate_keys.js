import React, { useState, useEffect } from 'react'
import Header from '../components/header'
import EC from 'elliptic'



const CalculateKeys = (props) => {

    const [selectedEcType, setSelectedEcType] = useState('ECDSA')
    const [selectedCurveType, setSelectedCurveType] = useState('secp256k1')
    const [keys, setKeys] = useState({
        privateKey: '',
        publicKey: { x: '', y: '' },
        loaded: false
    })
    const ecTypes = ['ECDSA', 'EdDSA', 'ECDH']
    const curveTypes = [
        'secp256k1',
        'p192',
        'p224',
        'p256',
        'p384',
        'p521',
        'curve25519',
        'ed25519'
    ]
    const ECEC = EC.ec
    const ECEdDSA = EC.eddsa

    const calculate = async () => {
        let ec, key
        switch (selectedEcType) {
            case 'ECDSA':
                ec = new ECEC(selectedCurveType.toLowerCase())
                key = ec.genKeyPair()
                const keysObject = {
                    privateKey: key.getPrivate().toString(),
                    publicKey: { x: key.getPublic().getX().toString(), y: key.getPublic().getY().toString() },
                    loaded: true
                }
                setKeys(keysObject)
                break
            case '':
                break
            default:
                break
        }
    }
    return (
        <div>
            <Header />
            <div className="hero">            
                <h1 className='title'>Calculate Keys</h1>
                <div className="row">
                    
                    <form onSubmit={e => {
                        e.preventDefault()
                        calculate()
                    }}>
                        <select value={selectedEcType} className="select-css" onChange={e => setSelectedEcType(e.target.value)}>
                            {ecTypes.map(ecType => <option key={ecType} value={ecType}>{ecType}</option>)}
                        </select>
                        <select value={selectedCurveType} className="select-css" onChange={e => setSelectedCurveType(e.target.value)}>
                            {curveTypes.map(curve => <option key={curve} value={curve}>{curve}</option>)}
                        </select>
                        <button className='button' type="submit">calculate</button>
                        <small>(only ECDSA for now)</small>
                    </form>
                </div>
                <div className='row'>
                    {keys.loaded ?
                        <div>
                            <p>
                                private key: {keys.privateKey}
                            </p>
                            <p>
                                public key: <br />
                                X: {keys.publicKey.x}<br />
                                Y: {keys.publicKey.y}
                            </p>
                        </div> : <p>calculate private and public key after selecting a ECC algorithm and a curve!</p>}
                </div>
            </div>
            <style jsx>{`
    .hero {
        font-family: sans-serif;
        width: 100%;
        color: #333;
      }
      .title {
        margin: 0;
        width: 100%;
        padding-top: 80px;
        line-height: 1.15;
        font-size: 30px;
      }
      .title,
      .description {
        text-align: center;
      }
      .row {
        max-width: 880px;
        margin: 80px auto 40px;
        display: flex;
        flex-direction: row;
        justify-content: space-around;
      }
      .card {
        padding: 18px 18px 24px;
        width: 220px;
        text-align: left;
        text-decoration: none;
        color: #434343;
        border: 1px solid #9b9b9b;
      }
      .card:hover {
        border-color: #067df7;
      }
      .card h3 {
        margin: 0;
        color: #067df7;
        font-size: 18px;
      }
      .card p {
        margin: 0;
        padding: 12px 0 0;
        font-size: 13px;
        color: #333;
      }
    .select-css {
            margin: 20px;
        display: block;
        font-size: 16px;
        font-family: sans-serif;
        font-weight: 700;
        color: #444;
        line-height: 1.3;
        padding: .6em 1.4em .5em .8em;
        width: 15vh;
        height: 5vh;
        box-sizing: border-box;
        margin: 0;
        border: 1px solid #aaa;
        box-shadow: 0 1px 0 1px rgba(0,0,0,.04);
        border-radius: .5em;
        -moz-appearance: none;
        -webkit-appearance: none;
        appearance: none;
        background-color: #fff;
        background-image: url('data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22292.4%22%20height%3D%22292.4%22%3E%3Cpath%20fill%3D%22%23007CB2%22%20d%3D%22M287%2069.4a17.6%2017.6%200%200%200-13-5.4H18.4c-5%200-9.3%201.8-12.9%205.4A17.6%2017.6%200%200%200%200%2082.2c0%205%201.8%209.3%205.4%2012.9l128%20127.9c3.6%203.6%207.8%205.4%2012.8%205.4s9.2-1.8%2012.8-5.4L287%2095c3.5-3.5%205.4-7.8%205.4-12.8%200-5-1.9-9.2-5.5-12.8z%22%2F%3E%3C%2Fsvg%3E'),
            linear-gradient(to bottom, #ffffff 0%,#e5e5e5 100%);
        background-repeat: no-repeat, repeat;
        background-position: right .7em top 50%, 0 0;
        background-size: .65em auto, 100%;
        }
        .select-css::-ms-expand {
        display: none;
        }
        .select-css:hover {
        border-color: #888;
        }
        .select-css:focus {
        border-color: #aaa;
        box-shadow: 0 0 1px 3px rgba(59, 153, 252, .7);
        box-shadow: 0 0 0 3px -moz-mac-focusring;
        color: #222;
        outline: none;
        }
    .select-css option {
      font-weight:normal;
    }
    .button {
      -webkit-transition-duration: 0.4s; /* Safari */
      transition-duration: 0.4s;
      background-color: white;
      color: black;
      border: 2px solid #555555;
      width: 80px;
      height: 40px;
    }

    .button:hover {
      background-color: #555555;
      color: white;
      cursor: pointer;
    }
    `}</style>
        </div>
    )
}

export default CalculateKeys