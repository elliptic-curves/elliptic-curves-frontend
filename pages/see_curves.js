import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Header from '../components/header'



const SeeCurves = (props) => {

    const [selectedCurveType, setSelectedCurveType] = useState('simpleCurve')
    const [imageData, setImageData] = useState(null)
    const [curveA, setCurveA] = useState(3)
    const [curveB, setCurveB] = useState(5)

    useEffect(() => {
        const makeFetch = async () => {
            const res = await axios.post('https://elliptic-curves-api.herokuapp.com/calc_curve', {
                curve: curveTypes[selectedCurveType]
            });
            const body = await res.data
            setImageData(body['photo'])
        }
        makeFetch()
    }, [curveA, curveB, selectedCurveType])

    const curveTypes = {
        'simpleCurve': {
            equation: {
                string: `y^2 = x^3 + ${curveA}x + ${curveB}`,
                x3: 1,
                x2: 0,
                ax: curveA, //a
                b: curveB
            }
        },
        'secp256k1': {
            equation: {
                string: 'y^2 = x^3 + 7',
                x3: 1,
                x2: 0,
                ax: 0, //a
                b: 7
            }
        },
        'p192': { //testing
            equation: {
                string: 'y^2 = x^3 +ax + b',
                x3: 1,
                x2: -3,
                ax: 3,
                b: -1
            }
        },
        'p224': {
            equation: {
                string: 'y^2 = x^3-3x+18958286285566608000408668544493926415504680968679321075787234672564',
                x3: 1,
                x2: 0,
                ax: -3,
                b: 18958286285566608000408668544493926415504680968679321075787234672564
            }
        },
        'p256': {
            equation: {
                string: 'y^2 = x^3-3x+41058363725152142129326129780047268409114441015993725554835256314039467401291',
                x3: 1,
                x2: 0,
                ax: -3,
                b: 41058363725152142129326129780047268409114441015993725554835256314039467401291
            }
        },
        'p384': {
            equation: {
                string: 'y^2 = x^3-3x+27580193559959705877849011840389048093056905856361568521428707301988689241309860865136260764883745107765439761230575',
                x3: 1,
                x2: 0,
                ax: -3,
                b: 27580193559959705877849011840389048093056905856361568521428707301988689241309860865136260764883745107765439761230575
            }
        },
        'p521': {
            equation: {
                string: ''
            }
        },
        'curve25519': {
            equation: {
                string: 'y^2 = x^3+486662x^2+x',
                x3: 1,
                x2: 486662,
                ax: 1,
                b: 0
            }
        },
        'ed25519': {
            equation: {
                string: 'x^2+y^2 = 1-376014x^2y^2'
            }
        }
    }
    
    return (
        <div>
            <Header />
            <div className='hero'>
                <h1 className='title'>See Curves</h1>
                <p className='small_center'>give it a time to load the first image after selecting the curve, im using heroku free plan, may take a while before machine starts</p>
                <div className='row'>
                    <p>just change the type of the row and the curve updates automatically <small className="small">(the last ones still don't show because the scale is too small for those curves)</small></p>
                </div>
                <div className='row'>
                    <select value={selectedCurveType} className="select-css" onChange={e => setSelectedCurveType(e.target.value)}>
                        {Object.keys(curveTypes).map(curve => <option key={curve} value={curve}>{curve}</option>)}
                    </select>
                    {selectedCurveType === 'simpleCurve' ?
                        <div>
                            a({curveA}): <input type="range" min={-10} max={10} value={curveA} onInput={e => setCurveA(e.target.value)} onChange={e => setCurveA(e.target.value)}/>
                            <br></br>
                            b({curveB}): <input type="range" min={-10} max={10} value={curveB} onInput={e => setCurveB(e.target.value)} onChange={e => setCurveB(e.target.value)}/>
                            {
                            //a: <input type="number" value={curveA} max={10} min={-10} onChange={e => setCurveA(e.target.value)} />
                            //b: <input type="number" value={curveB} max={10} min={-10} onChange={e => setCurveB(e.target.value)} />
                            }
                        </div>
                    : null}
                    <p><strong>curve equation:</strong> {curveTypes[selectedCurveType].equation['string']}</p>
                    {imageData ? <img src={'data:;base64,' + imageData} height="300" width="300" /> : <div><img src='/empty_graphic.png' height="300" width="300" /></div>}
                </div>
                <style jsx>{`

                    .hero {
                        font-family: sans-serif;
                        width: 100%;
                        color: #333;
                    }
                    .title {
                        margin: 0;
                        width: 100%;
                        padding-top: 80px;
                        line-height: 1.15;
                        font-size: 30px;
                    }
                    .small_center {
                        font-size: 10px;
                        text-align: center;
                    }
                    .title,
                    .description {
                        text-align: center;
                    }
                    .row {
                        max-width: 880px;
                        margin: 80px auto 40px;
                        display: flex;
                        flex-direction: row;
                        justify-content: space-around;
                    }
                    .card {
                        padding: 18px 18px 24px;
                        width: 220px;
                        text-align: left;
                        text-decoration: none;
                        color: #434343;
                        border: 1px solid #9b9b9b;
                    }
                    .card:hover {
                        border-color: #067df7;
                    }
                    .card h3 {
                        margin: 0;
                        color: #067df7;
                        font-size: 18px;
                    }
                    .card p {
                        margin: 0;
                        padding: 12px 0 0;
                        font-size: 13px;
                        color: #333;
                    }
                    .select-css {
                        margin: 20px;
                    display: block;
                    font-size: 16px;
                    font-family: sans-serif;
                    font-weight: 700;
                    color: #444;
                    line-height: 1.3;
                    padding: .6em 1.4em .5em .8em;
                    width: 15vh;
                    height: 5vh;
                    box-sizing: border-box;
                    margin: 0;
                    border: 1px solid #aaa;
                    box-shadow: 0 1px 0 1px rgba(0,0,0,.04);
                    border-radius: .5em;
                    -moz-appearance: none;
                    -webkit-appearance: none;
                    appearance: none;
                    background-color: #fff;
                    background-image: url('data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22292.4%22%20height%3D%22292.4%22%3E%3Cpath%20fill%3D%22%23007CB2%22%20d%3D%22M287%2069.4a17.6%2017.6%200%200%200-13-5.4H18.4c-5%200-9.3%201.8-12.9%205.4A17.6%2017.6%200%200%200%200%2082.2c0%205%201.8%209.3%205.4%2012.9l128%20127.9c3.6%203.6%207.8%205.4%2012.8%205.4s9.2-1.8%2012.8-5.4L287%2095c3.5-3.5%205.4-7.8%205.4-12.8%200-5-1.9-9.2-5.5-12.8z%22%2F%3E%3C%2Fsvg%3E'),
                        linear-gradient(to bottom, #ffffff 0%,#e5e5e5 100%);
                    background-repeat: no-repeat, repeat;
                    background-position: right .7em top 50%, 0 0;
                    background-size: .65em auto, 100%;
                    }
                    .select-css::-ms-expand {
                    display: none;
                    }
                    .select-css:hover {
                    border-color: #888;
                    }
                    .select-css:focus {
                    border-color: #aaa;
                    box-shadow: 0 0 1px 3px rgba(59, 153, 252, .7);
                    box-shadow: 0 0 0 3px -moz-mac-focusring;
                    color: #222;
                    outline: none;
                    }
                    .select-css option {
                    font-weight:normal;
                    }
                    .button {
                    -webkit-transition-duration: 0.4s; /* Safari */
                    transition-duration: 0.4s;
                    background-color: white;
                    color: black;
                    border: 2px solid #555555;
                    width: 80px;
                    height: 40px;
                    }

                    .button:hover {
                    background-color: #555555;
                    color: white;
                    cursor: pointer;
                    }
                    `}</style>
            </div>
        </div>
    )
}

export default SeeCurves