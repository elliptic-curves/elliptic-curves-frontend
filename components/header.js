import Link from 'next/link';

const Header = () => (
    <div className="header-wrapper">
        <nav>
            <Link href="/">
                <a>Home</a>
            </Link>
            <Link href="/see_curves">
                <a>See Curves</a>
            </Link>
            <Link href="/calculate_keys">
                <a>Calculate Keys</a>
            </Link>
            <a target="_blank" href="https://gitlab.com/elliptic-curves">Source Code</a>
        </nav>

        <style jsx>{`
      .header-wrapper {
        padding: 20px;
        background: #9b9b9b;
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06);
      }
      a {
        padding: 10px 0;
        margin: 0 15px 0 0;
        white-space: nowrap;
        color: #fff;
        textDecoration: none;
        font-family: sans-serif;
        text-decoration: none;
      }
      a:hover {
        color: #000;
      }
    `}</style>
    </div>
);

export default Header;