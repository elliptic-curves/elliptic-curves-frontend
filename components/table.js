import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
    root: {
      width: '100%',
      overflowX: 'auto',
    },
    table: {
      minWidth: 550,
    },
});

function createData(description, type1, type2, type3) {
    return { description, type1, type2, type3 };
}




const TableComponent = ({dataColumns, dataRows}) => {
    const classes = useStyles();
    const rows = dataRows.map(row => createData(row['Description'], row['Symetric'], row['RSA'], row['ECC']))

    return (
        <Paper className={classes.root}>
        <Table className={classes.table} aria-label="simple table">
            <TableHead>
            <TableRow>
                <TableCell>{dataColumns[0]}</TableCell>
                {dataColumns.slice(1).map(column => <TableCell align="right" key={column}>{column}</TableCell>)}
            </TableRow>
            </TableHead>
            <TableBody>
            {rows.map(row => (
                <TableRow key={row.description}>
                <TableCell component="th" scope="row">
                    {row.description}
                </TableCell>
                <TableCell align="right">{row.type1}</TableCell>
                <TableCell align="right">{row.type2}</TableCell>
                <TableCell align="right">{row.type3}</TableCell>
                </TableRow>
            ))}
            </TableBody>
        </Table>
        </Paper>
    );
}          

export default TableComponent