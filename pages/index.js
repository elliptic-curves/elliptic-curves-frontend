import React, {useState, useEffect} from 'react'
import Header from '../components/header'
import Link from 'next/link';
import TableComponent from '../components/table'
import { Button } from '@material-ui/core';

var tableData = {
  columns: ['Description', 'Symetric', 'RSA', 'ECC'],
  rows: [{
    'Description': 'Adequate from this number of bits',
    'Symetric': 80,
    'RSA': 1024,
    'ECC': 160
  },
  {
    'Description': 'Minimum security',
    'Symetric': 112,
    'RSA': 2048,
    'ECC': 224
  },
  {
    'Description': 'Not adequate for top secret documents',
    'Symetric': 128,
    'RSA': 3072,
    'ECC': 256
  },
  {
    'Description': 'Used for top secret documents',
    'Symetric': 192,
    'RSA': 7680,
    'ECC': 384
  },
  {
    'Description': 'Security above top secret documents',
    'Symetric': 256,
    'RSA': 15360,
    'ECC': 521
  },
]
};

const FirstPage = () => (
  <div>
    <div className='row'>
      <p>The next generation of public key cryptography because of the key size</p>
      <TableComponent dataColumns={tableData.columns} dataRows={tableData.rows}/>
    </div>
    <style>{`
    .row {
      max-width: 880px;
      margin: 80px auto 40px;
      display: flex;
      flex-direction: row;
      justify-content: space-around;
    }`}</style>
  </div>
)

const SecondPage = () => (
  <div className='container'>
    <div className='row'>
      <p><strong>trapdoor: </strong>For this type of algorithm it is needed a mathematical function that makes one way easy but the other hard (ideally impossible). The more opposite those two ends are the better, i.e., the easier it is to go one way and the harder it is to reverse the better. </p>
    </div>
    <div className='row'>
      <p><strong>elliptic curve: </strong>An elliptic curve is the set of points that satisfy something similar to the following equation and graphic: <code>y2 = x3 + ax + b</code> </p>
      <img src="/elliptic_curve.png" alt="elliptic curve" width="270" height="200" />
    </div>
    <Link href="/see_curves" >
      <a className="center">See More Curves here</a>
    </Link>
    <div className='row'>
      <p><strong>“Dot” Operation: </strong>we specify a base point on the curve and add that point to itself. 1•A = A; 2•A = B; 3•A = C;...</p>
      <img src="/curve_intersections.gif" alt="curve intersections" width="270" height="250" />
    </div>
    <div className="center">
      <p>Here, the trapdoor is n*I=F, where n is the number the initial point "I" gets "dotted". The result is "F"</p>
    </div>
    
    <style>{`
    .row {
      max-width: 880px;
      margin: 80px auto 40px;
      display: flex;
      flex-direction: row;
      justify-content: space-around;
    }
    .center {
      margin: 80px auto 40px;
      display: flex;
      justify-content: space-around;
    }
    a {
      text-decoration: none;
      color: #067df7;
    }
    a:hover {
      color: #0600ff;
    }
    `}</style>
  </div>
)

const ThirdPage = () => (
  <div className='row'>
        <img src="/curve_intersections.gif" alt="curve intersections" width="300" height="300" />
        <img src="/multiple_points_intersection.gif" alt="multiple points intersections" width="400" height="300" />
  </div>
)

const CustomButton = ({currentIndex, index, setIndex}) => {
  return (
  <>
    {currentIndex === index ? <Button variant="contained" color="primary">
        {index+1}
      </Button> : <Button variant="contained" onClick={setIndex}>
        {index+1}
      </Button>}
  </>
  )
}
  
const pages = [<FirstPage />, <SecondPage />, <ThirdPage />]

const Home = (props) => {
  const [indexPage, setIndexPage] = useState(0);

  return (
    <div>
      <Header />

      <div className='hero'>
        <h1 className='title'>Learning Elliptic-Curve Cryptography</h1>
        <p className='description'>
          Attempt to explain the Elliptic-Curve cryptography
        </p>

        <div className='row'>
          <p>
          Check <Link href="/see_curves">
              <a>See Curves</a>
            </Link> and <Link href="/calculate_keys">
              <a>Calculate Keys</a>
            </Link> pages!
          </p>
        </div>
        <div className="page">
          {pages[indexPage]}
        </div>
        <div className="row">
          <CustomButton index={0} currentIndex={indexPage} setIndex={() => setIndexPage(0)}/>
          <CustomButton index={1} currentIndex={indexPage} setIndex={() => setIndexPage(1)}/>
          <CustomButton index={2} currentIndex={indexPage} setIndex={() => setIndexPage(2)}/>
        </div>
      </div>

      <style jsx>{`
      .hero {
        font-family: sans-serif;
        width: 100%;
        color: #333;
      }
      a {
        text-decoration: none;
        color: #067df7;
      }
      a:hover {
        color: #0600ff;
      }
      .title {
        margin: 0;
        width: 100%;
        padding-top: 80px;
        line-height: 1.15;
        font-size: 48px;
      }
      .title,
      .description {
        text-align: center;
      }
      .row {
        max-width: 880px;
        margin: 20px auto 10px;
        display: flex;
        flex-direction: row;
        justify-content: space-around;
      }
      .card {
        padding: 18px 18px 24px;
        width: 220px;
        text-align: left;
        text-decoration: none;
        color: #434343;
        border: 1px solid #9b9b9b;
      }
      .card:hover {
        border-color: #067df7;
      }
      .card h3 {
        margin: 0;
        color: #067df7;
        font-size: 18px;
      }
      .card p {
        margin: 0;
        padding: 12px 0 0;
        font-size: 13px;
        color: #333;
      }
      .select-css {
        margin: 20px;
      display: block;
      font-size: 16px;
      font-family: sans-serif;
      font-weight: 700;
      color: #444;
      line-height: 1.3;
      padding: .6em 1.4em .5em .8em;
      width: 100%;
      max-width: 100%;
      box-sizing: border-box;
      margin: 0;
      border: 1px solid #aaa;
      box-shadow: 0 1px 0 1px rgba(0,0,0,.04);
      border-radius: .5em;
      -moz-appearance: none;
      -webkit-appearance: none;
      appearance: none;
      background-color: #fff;
      background-image: url('data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22292.4%22%20height%3D%22292.4%22%3E%3Cpath%20fill%3D%22%23007CB2%22%20d%3D%22M287%2069.4a17.6%2017.6%200%200%200-13-5.4H18.4c-5%200-9.3%201.8-12.9%205.4A17.6%2017.6%200%200%200%200%2082.2c0%205%201.8%209.3%205.4%2012.9l128%20127.9c3.6%203.6%207.8%205.4%2012.8%205.4s9.2-1.8%2012.8-5.4L287%2095c3.5-3.5%205.4-7.8%205.4-12.8%200-5-1.9-9.2-5.5-12.8z%22%2F%3E%3C%2Fsvg%3E'),
        linear-gradient(to bottom, #ffffff 0%,#e5e5e5 100%);
      background-repeat: no-repeat, repeat;
      background-position: right .7em top 50%, 0 0;
      background-size: .65em auto, 100%;
    }
    .select-css::-ms-expand {
      display: none;
    }
    .select-css:hover {
      border-color: #888;
    }
    .select-css:focus {
      border-color: #aaa;
      box-shadow: 0 0 1px 3px rgba(59, 153, 252, .7);
      box-shadow: 0 0 0 3px -moz-mac-focusring;
      color: #222;
      outline: none;
    }
    .select-css option {
      font-weight:normal;
    }
    .button {
      -webkit-transition-duration: 0.4s; /* Safari */
      transition-duration: 0.4s;
      background-color: white;
      color: black;
      border: 2px solid #555555;
      width: 80px;
      height: 40px;
    }

    .button:hover {
      background-color: #555555;
      color: white;
      cursor: pointer;
    }
    `}</style>
    </div>
  )
}

/*Home.getInitialProps = async function () {
  //const res = await axios.get('http://localhost:5000/get_image');
  //const body = await res.data
  //return { image: body['photo'] }
};*/


export default Home
